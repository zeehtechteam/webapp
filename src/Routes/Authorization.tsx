import * as React from 'react';
import {inject, observer} from 'mobx-react';
import {Redirect} from 'react-router';


const Authorization = (allowedRoles: any) => (WrappedComponent: any) =>
  {
    return inject('userStore')(observer((props: any) => {
      if (allowedRoles.includes(props.userStore.role)) {
        return (<WrappedComponent {...props} />);
      } else {
        return <Redirect to='/login'/>
      }
    }));
    
  }

export default Authorization;