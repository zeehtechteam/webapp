// @ts-ignore
import React, { Suspense, lazy } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Authorization from "./Authorization";

const LoginPage = React.lazy(() => import("../pages/User/Login"));
const HomePage = React.lazy(() => import("../pages/HomePage"));
const RegisterPage = React.lazy(() => import("../pages/User/Register"));
const Dashboard = React.lazy(() => import("../pages/Dashboard"));
const PPRA = React.lazy(() => import("../pages/PPRA"));

const Loading = () => <div>Carregando...</div>;

const User = Authorization(["user"]);
const Admin = Authorization(["admin"]);
const AnyLogged = Authorization(["user", "admin"]);

const Routes = () => (
  <Suspense falling={<div>Loading...</div>}>
    <Router>
      <Switch>
        <Route exact path="/" component={WaitingComponent(HomePage)} />
        <Route path="/login" component={WaitingComponent(LoginPage)} />
        <Route path="/register" component={WaitingComponent(RegisterPage)} />
        <Route path="/dashboard" component={WaitingComponent(Dashboard)} />
        <Route path="/ppra" component={WaitingComponent(PPRA)} />
      </Switch>
    </Router>
  </Suspense>
);

function WaitingComponent(Component: React.Component) {
  return (props: any) => (
    <Suspense fallback={<div>Loading...</div>}>
      <Component {...props} />
    </Suspense>
  );
}

export default Routes;
