import * as React from "react";
import FormField from "../../../form-management/FormField";
import * as FormManagement from "../../../form-management/FormManagement";
import { inject, observer } from "mobx-react";
import ModalStore from "../../Modal/store";
import { Query } from "react-apollo";
import GET_DATA from "../../../graphql/queries/GetData";

export interface FormBuilderFieldProps {
  field: FormField;
  modalStore?: ModalStore;
}

const getType = (field: FormField) => {
  if (field instanceof FormManagement.TextField) return "text";
  if (field instanceof FormManagement.NumericField) return "numeric";
  if (field instanceof FormManagement.CheckField) return "check";
  if (field instanceof FormManagement.RadioField) return "radio";
  if (field instanceof FormManagement.MultipleField) return "multiple";
  if (field instanceof FormManagement.SelectField) return "select";
  return "undefined";
};

@inject("modalStore")
@observer
class FormBuilderField extends React.Component<FormBuilderFieldProps, any> {
  constructor(props: FormBuilderFieldProps) {
    super(props);
  }
  render() {
    const field = this.props.field as any;

    const renderer =
      field.relatedForm === undefined ? (
        // Formulário comum
        getType(field) === "text" || getType(field) === "numeric" ? (
          // Tipo text ou numeric
          <div>
            <label className="label">{field.title}</label>
            <div className="control is-expanded">
              <input
                className="input"
                type="text"
                value={field.value || ""}
                onChange={e => (field.value = e.target.value)}
              />
            </div>
          </div>
        ) : getType(field) === "check" ? (
          // Tipo check
          <div>
            <label className="checkbox">
              <input
                type="checkbox"
                value={field.value}
                onChange={e => (field.value = e.target.value)}
              />
              {field.title}
            </label>
          </div>
        ) : getType(field) === "select" ? (
          // Tipo select
          <div>
            <label className="label">{field.title}</label>
            <div className="control">
              <div className="select">
                <select
                  value={field.value}
                  onChange={e => (field.value = e.target.value)}
                >
                  {field.availableValues.map((val: any) => (
                    <option key={val.value + "-"} value={val.value}>
                      {val.displayValue}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        ) : (
          // Caso não tenha sido tratado o tipo
          <div>Não encontrado</div>
        )
      ) : // Formulário relacionado
      getType(field) === "select" ? (
        <Query
          query={GET_DATA}
          variables={{ dataType: field.relatedForm.dbTable }}
        >
          {({ loading, error, data }) => {
            if (loading) {
              return <div>Carregando...</div>;
            }
            if (error) {
              return <div>Algum erro ocorreu</div>;
            }
            field.relatedForm.data = data.getData;
            field.relatedForm.DataRefreshedCallback();
            return (
              <div>
                <label className="label">{field.title}</label>
                <div className="control">
                  <div className="select">
                    <select
                      value={field.value}
                      onChange={e => (field.value = e.target.value)}
                    >
                      {field.availableValues.map((val: any) => (
                        <option
                          key={"field" + val.displayValue + "-"}
                          value={val.value}
                        >
                          {val.displayValue}
                        </option>
                      ))}
                    </select>
                  </div>
                </div>
                <button
                  type="button"
                  className="button is-info"
                  onClick={() =>
                    (this.props.modalStore.innerModal = field.relatedForm)
                  }
                >
                  Gerenciar
                </button>
              </div>
            );
          }}
        </Query>
      ) : (
        // Caso não tenha sido tratado
        <div>Não encontrado</div>
      );

    return (
      <div className={"column is-" + field.columns.computer}>{renderer}</div>
    );
  }
}

export default FormBuilderField;
