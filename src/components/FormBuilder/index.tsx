import * as React from "react";
import { observer, Observer } from "mobx-react";
import Form from "../../form-management/Form";
import FormBuilderField from "./Field";
import FormBuilderStore from "./store";
import "./style.scss";
import { Query, Mutation, MutationFn, OperationVariables } from "react-apollo";
import GET_DATA from "../../graphql/queries/GetData";
import DATA_ADD from "../../graphql/mutations/DataAdd";
import DATA_REMOVE from "../../graphql/mutations/DataRemove";
import DATA_UPDATE from "../../graphql/mutations/DataUpdate";

export interface FormBuilderProps {
  form: Form;
}

@observer
class FormBuilder extends React.Component<FormBuilderProps, any> {
  public store: FormBuilderStore;
  constructor(props: FormBuilderProps) {
    super(props);
    this.store = new FormBuilderStore();
  }

  dataAdd = (mutation: MutationFn<any, OperationVariables>, refetch: any) => {
    const { form } = this.props;
    form.saving = true;
    mutation({
      variables: {
        dataType: form.dbTable,
        payload: form.getPayload()
      }
    }).finally(() => {
      form.saving = false;
      form.loading = true;
      form.resetFields();
      refetch().finally(() => {
        form.loading = false;
      });
    });
  };

  dataRemove = (
    mutation: MutationFn<any, OperationVariables>,
    refetch: any,
    item: any
  ) => {
    const { form } = this.props;
    form.deletingIds.push(item.id);
    mutation({
      variables: {
        dataType: form.dbTable,
        payload: item
      }
    }).finally(() => {
      form.deletingIds.filter(id => id !== item.id);
      if (form.editingId === item.id) {
        form.resetFields();
      }
      form.loading = true;
      refetch().finally(() => {
        form.loading = false;
      });
    });
  };

  dataUpdate = (
    mutation: MutationFn<any, OperationVariables>,
    refetch: any
  ) => {
    const { form } = this.props;
    form.saving = true;
    mutation({
      variables: {
        dataType: form.dbTable,
        payload: { ...form.getPayload(), id: form.editingId }
      }
    }).finally(() => {
      form.saving = false;
      form.loading = true;
      form.resetFields();
      refetch().finally(() => {
        form.loading = false;
      });
    });
  };

  render() {
    const store = this.store;
    const form = this.props.form;

    const addButton = (refetch: any) =>
      form.editingId === "" ? (
        <Mutation mutation={DATA_ADD}>
          {dataAddAction => (
            <Observer>
              {() => (
                <button
                  className={
                    "button is-success " +
                    (form.saving === true ? "is-loading" : "")
                  }
                  type="button"
                  // @ts-ignore
                  onClick={() => this.dataAdd(dataAddAction, refetch)}
                >
                  Adicionar
                </button>
              )}
            </Observer>
          )}
        </Mutation>
      ) : (
        <Mutation mutation={DATA_UPDATE}>
          {dataUpdateAction => (
            <Observer>
              {() => (
                <button
                  className={
                    "button is-success " +
                    (form.saving === true ? "is-loading" : "")
                  }
                  type="button"
                  // @ts-ignore
                  onClick={() => this.dataUpdate(dataUpdateAction, refetch)}
                >
                  Salvar
                </button>
              )}
            </Observer>
          )}
        </Mutation>
      );

    const removeButton = (refetch: any, item: any) => (
      <Mutation mutation={DATA_REMOVE}>
        {dataRemoveAction => (
          <Observer>
            {() => (
              <button
                className={
                  "button is-danger" +
                  (form.deletingIds.some(x => x === item.id)
                    ? " is-loading"
                    : "")
                }
                type="button"
                onClick={() => this.dataRemove(dataRemoveAction, refetch, item)}
              >
                Excluir
              </button>
            )}
          </Observer>
        )}
      </Mutation>
    );

    return (
      <Query
        query={GET_DATA}
        variables={{
          dataType: form.dbTable
        }}
      >
        {({ data, error, loading, refetch }) => {
          if (loading || error) {
            return <div>Carregando...</div>;
          }
          form.data = data.getData;
          const fields = form.fields.map(field => (
            <FormBuilderField key={field.title} field={field} />
          ));
          return (
            <Observer>
              {() => (
                <div>
                  <div className="card">
                    <div className="card-header">
                      <h3 className="card-header-title">{form.singleTitle}</h3>
                    </div>
                    <div className="card-content columns is-multiline">
                      {fields}
                      {addButton(refetch)}
                      {form.editingId === "" ? (
                        <button
                          className="button is-warning"
                          type="button"
                          onClick={() => form.resetFields()}
                        >
                          Limpar Campos
                        </button>
                      ) : (
                        <button
                          className="button is-danger"
                          type="button"
                          onClick={() => form.resetFields()}
                        >
                          Cancelar
                        </button>
                      )}
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header">
                      <h3 className="card-header-title">
                        Lista de {form.pluralTitle}
                      </h3>
                    </div>
                    <div className="card-content">
                      <div className="div-tabela">
                        <table className="table is-fullwidth is-bordered is-striped">
                          <thead>
                            <tr>
                              {form.tableVisibleFields.map((field, key) => (
                                <th key={form.dbTable + field.dbColumn + key}>
                                  {field.title}
                                </th>
                              ))}
                              <th />
                              <th />
                            </tr>
                          </thead>
                          <tbody>
                            {form.data
                              .slice()
                              .sort((d1: any, d2: any) => d1.id - d2.id)
                              .map(item => (
                                <tr
                                  key={
                                    form.dbTable + form.pluralTitle + item.id
                                  }
                                >
                                  {form.tableVisibleFields.map(field => (
                                    <td key={field.dbColumn + field.title}>
                                      {item[field.dbColumn]}
                                    </td>
                                  ))}
                                  <td className="is-narrow">
                                    <button
                                      className="button is-warning"
                                      type="button"
                                      onClick={() => form.editItem(item)}
                                    >
                                      Editar
                                    </button>
                                  </td>
                                  <td className="is-narrow">
                                    {removeButton(refetch, item)}
                                  </td>
                                </tr>
                              ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </Observer>
          );
        }}
      </Query>
    );
  }
}

export default FormBuilder;
