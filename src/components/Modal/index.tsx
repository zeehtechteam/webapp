import './style.scss';
import * as React from 'react';
import { CSSTransition } from 'react-transition-group';
import { observer, inject, Provider } from 'mobx-react';
import ModalStore from './store';
import Form from '../../form-management/Form';
import Empresas from '../../forms/Empresas';
import FormBuilder from '../FormBuilder';

export interface ModalProps {
  form: Form;
  onClose: () => void;
}

@observer
class Modal extends React.Component<ModalProps, any> {
    public store: ModalStore;
    constructor(props: ModalProps) {
        super(props);
        this.store = new ModalStore();
    }
    render() {
      const state = this.store;
      const form = this.props.form;
        return (
            <div>
                <CSSTransition timeout={3000} classNames="modaltransition" >
                    <div className="modal is-active">
                        <div className="modal-background"></div>

                        <div className="modal-card">
                            <header className="modal-card-head">
                                <p className="modal-card-title">Gerenciamento de {form.pluralTitle}</p>
                                <button className="delete" aria-label="close" onClick={this.props.onClose}></button>
                            </header>
                            <section className="modal-card-body">
                                <Provider modalStore={state}>
                                  <FormBuilder form={form}/>
                                </Provider>
                            </section>
                            <footer className="modal-card-foot">
                                <button className="button is-success" onClick={this.props.onClose}>Fechar</button>
                            </footer>
                        </div>

                        {state.innerModal !== undefined ? (
                          <Modal form={state.innerModal} onClose={() => state.innerModal = undefined} />
                        ) : 
                          <div></div>
                        }
                        {//<ModalManageItem ref="innerModal" v-if="InnerModal !== ''" TableName="Cnaes" />
                        }
                    </div>
                </CSSTransition>
            </div >
        );
    }
}

export default Modal;