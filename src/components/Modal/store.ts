import {observable, action, computed} from 'mobx';
import Form from '../../form-management/Form';

class ModalStore {
  @observable tableName: string;
  @observable innerModal: Form;
  constructor() {
    this.tableName = '';
  }
}

export default ModalStore;