import {observable as obs} from 'mobx';
import Form from './Form';
import IColumns from './IColumns';

interface FormFieldParameters {
    dbColumn: string;
    title: string;
    relatedForm?: Form;
    columns: IColumns;
}

class FormField {
    @obs public dbColumn: string;
    @obs public title: string;
    @obs public value: any;
    @obs public defaultValue: any;
    @obs public relatedForm: Form | undefined;
    @obs public columns: IColumns;
    constructor(params: FormFieldParameters) {
        this.dbColumn = params.dbColumn;
        this.title = params.title;
        this.relatedForm = params.relatedForm;
        this.columns = params.columns;
    }
}

export {FormFieldParameters};
export default FormField;
