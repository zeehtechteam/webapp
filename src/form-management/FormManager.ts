export interface BaseField {
    defaultValue: any;
    labelText: string;
    dbColumn: string;
}

export class TextField implements BaseField {
    public static fromObj(sourceObj: any) {
        const obj = new TextField();
        obj.defaultValue = sourceObj.defaultValue;
        obj.labelText = sourceObj.labelText;
        obj.dbColumn = sourceObj.dbColumn;
        return obj;
    }

    public defaultValue: any;
    public labelText: string;
    public dbColumn: string;
    constructor() {
        this.defaultValue = '';
        this.labelText = '';
        this.dbColumn = '';
    }
}

export interface BaseForm {
    title: string;
    table: string;
    campos: BaseField[];
}
