interface IColumns {
  mobile: number;
  tablet: number;
  computer: number;
}

export default IColumns;
