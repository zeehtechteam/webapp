import Form from './Form';
import FormField, {FormFieldParameters} from './FormField';
import IColumns from './IColumns';

interface TextFieldParameters {
    dbColumn: string;
    title: string;
    value: string;
    relatedForm?: Form;
    columns: IColumns;
}

class TextField extends FormField {
    public value: string;
    constructor(params: TextFieldParameters) {
        super({
            dbColumn: params.dbColumn,
            title: params.title,
            relatedForm: params.relatedForm,
            columns: params.columns,
        } as FormFieldParameters);
        this.value = params.value;
        this.defaultValue = params.value;
    }
}

export {TextFieldParameters};
export default TextField;
