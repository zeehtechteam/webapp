import Form, { FormParameters } from '../form-management/Form';
import { createDescricaoField } from '../forms/Forms';
import RiscosOcupacionais from '../forms/RiscosOcupacionais';
import SelectField, { SelectFieldParameters } from '../form-management/SelectField';

const AgentesDeRisco = new Form({
  dbTable: 'agentesDeRisco',
  singleTitle: 'Agente de Risco',
  pluralTitle: 'Agentes de Risco',
  fields: [
    createDescricaoField(),
    new SelectField({
      dbColumn: 'riscoOcupacional',
      title: 'Risco Ocupacional',
      relatedForm: RiscosOcupacionais,
      columns: {mobile: 12, tablet: 12, computer: 12},
    } as SelectFieldParameters),
  ],
  tableVisibleFields: [
    createDescricaoField(),
  ],
} as FormParameters);

export default AgentesDeRisco;
