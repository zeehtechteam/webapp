import Form, { FormParameters } from '../form-management/Form';
import { createTextField } from '../forms/Forms';

const Atuacoes = new Form({
  dbTable: 'atuacoes',
  singleTitle: 'Atuação',
  pluralTitle: 'Atuações',
  fields: [
      createTextField('medicoCoordenador', 'Médico Coordenador',
          {mobile: 12, tablet: 12, computer: 12}),
      createTextField('responsavelEmpresa', 'Responsável pela Empresa',
          {mobile: 12, tablet: 12, computer: 12}),
  ],
  tableVisibleFields: [
      createTextField('medicoCoordenador', 'Médico Coordenador',
          {mobile: 12, tablet: 12, computer: 12}),
      createTextField('responsavelEmpresa', 'Responsável pela Empresa',
          {mobile: 12, tablet: 12, computer: 12}),
  ],
} as FormParameters);

export default Atuacoes;
