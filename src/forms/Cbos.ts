import Form, { FormParameters } from '../form-management/Form';
import { createNomeField, createDescricaoField } from '../forms/Forms';

const Cbos = new Form({
  dbTable: 'cbos',
  singleTitle: 'CBO',
  pluralTitle: 'CBOs',
  fields: [
    createNomeField(),
    createDescricaoField(),
  ],
  tableVisibleFields: [
    createNomeField(),
    createDescricaoField(),
  ],
} as FormParameters);

export default Cbos;
