import Form, { FormParameters } from '../form-management/Form';
import { createDescricaoField } from '../forms/Forms';
import SelectField, { SelectFieldParameters } from '../form-management/SelectField';

const Cnaes = new Form({
  dbTable: 'cnaes',
  singleTitle: 'CNAE',
  pluralTitle: 'CNAEs',
  fields: [
    createDescricaoField(),
    new SelectField({
      dbColumn: 'grauDeRisco',
      title: 'Grau de Risco',
      availableValues: [
        {value: 0, displayValue: '0'},
        {value: 1, displayValue: '1'},
        {value: 2, displayValue: '2'},
        {value: 3, displayValue: '3'},
        {value: 4, displayValue: '4'},
      ],
      value: {value: 0},
      columns: {mobile: 12, tablet: 12, computer: 12},
    } as SelectFieldParameters),
  ],
  tableVisibleFields: [
    createDescricaoField(),
  ],
} as FormParameters);

export default Cnaes;
