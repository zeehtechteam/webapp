import {observable, action} from 'mobx';

class UserStore {
  @observable public loggedIn: boolean;
  @observable public role: string;
  constructor() {
    this.role = 'admin';
  }
  @action.bound public logout() {
    this.loggedIn = false;
    sessionStorage.removeItem('authkey');
  }
}

export default UserStore;