import gql from "graphql-tag";

export default gql`
  mutation DataAdd($dataType: String!, $payload: JSON) {
    dataAdd(input: { dataType: $dataType, payload: $payload })
  }
`;
