import gql from "graphql-tag";

export default gql`
  mutation DataRemove($dataType: String!, $payload: JSON) {
    dataRemove(input: { dataType: $dataType, payload: $payload })
  }
`;
