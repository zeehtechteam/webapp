import gql from "graphql-tag";

export default gql`
  mutation DataUpdate($dataType: String!, $payload: JSON) {
    dataUpdate(input: { dataType: $dataType, payload: $payload })
  }
`;
