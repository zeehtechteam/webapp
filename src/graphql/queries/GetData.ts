import gql from "graphql-tag";

export default gql`
  query GetData($dataType: String!) {
    getData(dataType: $dataType)
  }
`;
