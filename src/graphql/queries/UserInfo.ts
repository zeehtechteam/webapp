import gql from "graphql-tag";

export default gql`
  query UserInfo {
    userInfo {
      name
      email
      phone
      items {
        ppras
        pcmsos
      }
      transactions {
        id
        date
        approvedDate
        status
      }
    }
  }
`;
