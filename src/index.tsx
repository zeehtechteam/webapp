import * as React from "react";
import * as ReactDOM from "react-dom";
import axios from "axios";
import { Provider } from "mobx-react";
import App from "./App";
import "./style.scss";
import { title } from "./app-config";
import UserStore from "./global-stores/UserStore";
import { ApolloProvider } from "react-apollo";
import initApollo from "./init-apollo";

const userStore = new UserStore();

if (sessionStorage.getItem("authkey") !== null) {
  userStore.loggedIn = true;
}

document.title = title;

const client = initApollo();

ReactDOM.render(
  <Provider userStore={userStore}>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
  </Provider>,
  document.getElementById("app")
);
