import { ApolloClient } from "apollo-boost";
import { HttpLink } from "apollo-boost";
import { InMemoryCache } from "apollo-boost";
import { setContext } from "apollo-link-context";
import { createLoona } from "@loona/react";
import fetch from "isomorphic-unfetch";

function create() {
  const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem("auth");
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ""
      }
    };
  });
  const httpLink = new HttpLink({
    uri: "http://localhost:8085",
    credentials: "same-origin"
  });
  const cache = new InMemoryCache();
  const loonaState = createLoona(cache);
  return new ApolloClient({
    connectToDevTools: true,
    ssrMode: false, // Disables forceFetch on the server (so queries are only run once)
    link: authLink.concat(httpLink).concat(loonaState),
    cache: new InMemoryCache()
  });
}

export default function initApollo() {
  return create();
}
