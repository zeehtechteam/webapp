import {title as titlePrefix} from '../app-config';

export const setTitle = (title: string) => {
  document.title = titlePrefix + ' - ' + title;
};