import * as React from "react";
import field from "./classes/field";

type State = {
  empresa: string;
  quantidadeFuncionarios: number;
};
class Geral extends React.Component<{}, State> {
  initialState: State = {
    empresa: "",
    quantidadeFuncionarios: 0
  };
  state = this.initialState;

  changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    switch (e.target.name) {
      case "empresa": {
        this.setState({ empresa: e.target.value });
        break;
      }
      case "quantidadeFuncionarios": {
        let value = parseInt(e.target.value);
        if (isNaN(value)) {
          value = 0;
        }
        this.setState({ quantidadeFuncionarios: value });
        break;
      }
    }
  };
  render() {
    return (
      <>
        <div className={field}>
          <label>Empresa</label>
          <input
            type="empresa"
            placeholder="Empresa"
            name="empresa"
            value={this.state.empresa}
            onChange={this.changeHandler}
            autoFocus
          />
        </div>
        <div className={field}>
          <label>Quantidade de Funcionários</label>
          <input
            type="quantidadeFuncionarios"
            placeholder="Quantidade de Funcionários"
            name="quantidadeFuncionarios"
            value={this.state.quantidadeFuncionarios}
            onChange={this.changeHandler}
          />
        </div>
      </>
    );
  }
}

export default Geral;
