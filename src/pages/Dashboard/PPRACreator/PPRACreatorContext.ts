import * as React from "react";
import { Setor } from "./types";

export type PPRACreatorContextType = {
  Empresa: string;
  QuantidadeDeFuncionarios: number;
  Setores: Setor[];
};

export const PPRACreatorDefaultValue: PPRACreatorContextType = {
  Empresa: "",
  QuantidadeDeFuncionarios: 0,
  Setores: []
};

export default React.createContext<PPRACreatorContextType>(
  PPRACreatorDefaultValue
);
