import * as React from "react";
import field from "./classes/field";
import SetorItem from "./components/SetorItem";
import PPRACreatorStore from "./store";
import { inject, observer } from "mobx-react";
import { css } from "emotion";

type Props = {
  ppraCreator?: typeof PPRACreatorStore;
};
type State = {
  nomeSetor: string;
};

@inject("ppraCreator")
@observer
class Setores extends React.Component<Props, State> {
  initialState: State = {
    nomeSetor: ""
  };
  state = this.initialState;
  onChangeNomeSetor = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ nomeSetor: e.target.value });
  };
  addSetor = () => {
    this.props.ppraCreator.addSetor(this.state.nomeSetor);
    this.setState({ nomeSetor: "" });
  };
  render() {
    const { nomeSetor } = this.state;
    const { ppraCreator } = this.props;
    return (
      <div>
        <h1>Setores</h1>
        <div className={field}>
          <label>Nome do setor</label>
          <input
            type="text"
            value={nomeSetor}
            onChange={this.onChangeNomeSetor}
          />
        </div>
        <button onClick={this.addSetor}>Adicionar</button>
        <h5>Lista de Setores</h5>
        {ppraCreator.Setores.map((setor, key) => (
          <SetorItem key={`PPRACreatorSetor${key}`} setor={setor} />
        ))}
      </div>
    );
  }
}

export default Setores;
