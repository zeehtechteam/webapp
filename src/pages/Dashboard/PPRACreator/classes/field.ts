import { css } from "emotion";

export default css`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  & > label {
    font-weight: bold;
  }
  & > input {
    margin-top: 5px;
    padding: 10px;
    border-radius: 5px;
    border: 1px solid #dcdcdc;
  }
  margin: 10px 0px;
`;
