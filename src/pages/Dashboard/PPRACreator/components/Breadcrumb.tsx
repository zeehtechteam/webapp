import * as React from "react";
import { css } from "emotion";

const container = css`
  ul {
    list-style-type: none;
    display: flex;
    & > li {
      color: white;
      margin: 5px;
      padding: 10px 20px;
      background: lightblue;
      &.active {
        background: dodgerblue;
      }
    }
  }
`;

type Props = {
  paths: string[];
  pathIndex: number;
};
const Breadcrumb: React.SFC<Props> = ({ paths, pathIndex }) => {
  return (
    <div className={container}>
      <ul>
        {paths.map((path, key) => (
          <li className={pathIndex === key && "active"}>{path}</li>
        ))}
      </ul>
    </div>
  );
};

export default Breadcrumb;
