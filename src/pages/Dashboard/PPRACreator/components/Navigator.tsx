import * as React from "react";
import { css } from "emotion";

const container = css`
  margin: 20px;
  & > button {
    padding: 10px;
    background: dodgerblue;
    border: none;
    color: #fff;
    border-radius: 5px;
    margin-right: 10px;
    width: 70px;
    &:last-of-type {
      margin-right: 0;
    }
  }
`;

type Props = {
  onNext?: () => void;
  onPrevious?: () => void;
};
const Navigator: React.SFC<Props> = ({ onNext, onPrevious }) => (
  <div className={container}>
    {onPrevious && (
      <button type="button" onClick={onPrevious}>
        Voltar
      </button>
    )}
    {onNext && (
      <button type="button" onClick={onNext}>
        Avançar
      </button>
    )}
  </div>
);

export default Navigator;
