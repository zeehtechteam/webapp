import * as React from "react";
import { Spring, Transition, animated, interpolate } from "react-spring";
import { css } from "emotion";
import { FaPlus } from "react-icons/fa";
import field from "../classes/field";
import { Setor, Funcao } from "../store";
import { observer } from "mobx-react";
import Node from "./node";

const container = css`
  background: #fff;
  border: 1px solid dodgerblue;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  align-items: stretch;
  text-align: left;
  margin-bottom: 5px;
  overflow-y: hidden;
  & > header {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    background: dodgerblue;
    padding: 5px;
    cursor: pointer;
    color: #fff;
    & > h6 {
      margin: 0;
      padding: 0;
      color: #fff;
    }
    & > small {
      border: 1px solid #fff;
      color: #fff;
      margin: 0;
      background: dodgerblue;
      padding: 2px 10px;
      border-radius: 10px;
      font-size: 8px;
      margin-right: 5px;
      text-transform: uppercase;
      font-weight: 800;
    }
    & > .toggle-button {
      margin-left: auto;
      margin-right: 5px;
      font-size: 16px;
      font-weight: 600;
    }
  }
`;

const itemAdd = css`
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  align-items: stretch;
  box-sizing: border-box;
  margin: 5px;
  & > div {
    display: flex;
    & > span {
      font-weight: 600;
      color: #000;
    }
    & > input {
      margin: 0px 10px;
      width: 300px;
    }
  }
  & > h5 {
    margin: 0;
    padding: 0;
    margin-top: 10px;
    margin-bottom: 5px;
    color: #000;
    font-weight: 600;
  }
`;

type Props = {
  setor?: typeof Setor.Type;
  funcao?: typeof Funcao.Type;
};
type State = {
  opened: boolean;
  nomeSubItem: string;
};

@observer
class SetorItem extends React.Component<Props, State> {
  initialState: State = {
    opened: false,
    nomeSubItem: ""
  };
  state = this.initialState;
  toggleOpened = () => {
    this.setState(({ opened }) => ({ opened: !opened }));
  };
  addSubItem = () => {
    if (this.props.setor) {
      this.props.setor.add(this.state.nomeSubItem);
      this.setState({ nomeSubItem: "" });
    } else {
      this.props.funcao.add(this.state.nomeSubItem);
      this.setState({ nomeSubItem: "" });
    }
  };
  onChangeNomeSubItem = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ nomeSubItem: e.target.value });
  };
  render() {
    const { setor, funcao } = this.props;
    const { opened, nomeSubItem } = this.state;

    return (
      <div className={container}>
        <header onClick={this.toggleOpened}>
          <small>{setor ? "Setor" : "Função/Cargo"}</small>
          <h6>{setor ? setor.Nome : funcao.Nome}</h6>
          <Spring native from={{ rotate: 0 }} to={{ rotate: opened ? 45 : 0 }}>
            {({ rotate }) => (
              <animated.div
                style={{
                  transform: interpolate([rotate], r => `rotate(${r}deg)`)
                }}
                className="toggle-button"
              >
                {/* <FaPlus /> */}
                <FaPlus />
              </animated.div>
            )}
          </Spring>
        </header>
        <Transition
          native
          items={opened}
          from={{ scale: 0 }}
          leave={{ scale: 0 }}
          enter={{ scale: 200 }}
          config={{ duration: 300, friction: 1, tension: 1 }}
        >
          {opened =>
            opened &&
            (props => (
              <animated.div
                className={itemAdd}
                style={{
                  // transform: interpolate([props.scale], s => `scaleY(${s})`),
                  // transformOrigin: "top"
                  maxHeight: interpolate([props.scale], s => s)
                }}
              >
                <div>
                  <span>
                    {setor
                      ? "Nova Função ou Cargo:"
                      : "Novo Risco Ocupacional:"}
                  </span>
                  <input
                    className="input is-small"
                    type="text"
                    value={nomeSubItem}
                    onChange={this.onChangeNomeSubItem}
                  />
                  <button
                    className="button is-success is-small"
                    onClick={this.addSubItem}
                  >
                    Adicionar
                  </button>
                </div>
                <h5>
                  Lista de{" "}
                  {this.props.setor
                    ? "Funções e Cargos"
                    : "Riscos Ocupacionais"}
                </h5>
                {setor &&
                  this.props.setor.Funcoes.map((funcao, key) => (
                    <SetorItem
                      key={`PPRACreatorFuncao${key}`}
                      funcao={funcao}
                    />
                  ))}
                {funcao &&
                  this.props.funcao.RiscosOcupacionais.map(risco => (
                    <>{risco.Nome}</>
                  ))}
              </animated.div>
            ))
          }
        </Transition>
      </div>
    );
  }
}

export default SetorItem;
