import React, { Component } from "react";
import { css } from "emotion";
import { Setor, Funcao, RiscoOcupacional, AgenteDeRisco } from "../store";

const container = css`
  background: #fff;
  border: 1px solid dodgerblue;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  justify-content: stretch;
  align-items: stretch;
  text-align: left;
  margin-bottom: 5px;
  overflow-y: hidden;
  & > header {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    background: dodgerblue;
    padding: 5px;
    cursor: pointer;
    color: #fff;
    & > h6 {
      margin: 0;
      padding: 0;
      color: #fff;
    }
    & > small {
      border: 1px solid #fff;
      color: #fff;
      margin: 0;
      background: dodgerblue;
      padding: 2px 10px;
      border-radius: 10px;
      font-size: 8px;
      margin-right: 5px;
      text-transform: uppercase;
      font-weight: 800;
    }
    & > .toggle-button {
      margin-left: auto;
      margin-right: 5px;
      font-size: 16px;
      font-weight: 600;
    }
  }
`;

const compareType = (itemReceived: any, item: any) =>
  typeof typeof itemReceived === typeof item;

const onChangeField = (e: React.ChangeEvent<HTMLInputElement>) => (
  item: any,
  itemField: string
) => {
  item[itemField] = e.target.value;
};

const NamedInput: React.SFC<{
  item: any;
  description: string;
  fieldName: string;
}> = ({ item, description, fieldName }) => (
  <div className="field">
    <label>{description}</label>
    <input
      type="text"
      value={item[fieldName]}
      onChange={e => onChangeField(e)(item, item.setNome(e.target.value))}
    />
  </div>
);

type Props = {
  item: any;
  type: any;
};
type State = {
  opened: boolean;
};
class Node extends Component<Props, State> {
  initialState: State = {
    opened: true
  };
  state = this.initialState;
  editingItem = this.props.type.create({});

  render() {
    const { item, type } = this.props;
    const isSetor = compareType(type, Setor);
    const isFuncao = compareType(type, Funcao);
    const isRiscoOcupacional = compareType(type, RiscoOcupacional);
    const isAgenteDeRisco = type === compareType(type, AgenteDeRisco);
    const [singleName, pluralName, childType] = isSetor
      ? ["Setor", "Setores", "funcao"]
      : isFuncao
      ? ["Função", "Funções", "riscoOcupacional"]
      : isRiscoOcupacional
      ? ["Risco Ocupacional", "Riscos Ocupacionais", "agenteDeRisco"]
      : isAgenteDeRisco
      ? ["Agente de Risco", "Agentes de Risco", ""]
      : ["", "", ""];

    return (
      <div className={container}>
        <h1>{pluralName}</h1>
        <h5>Novo {singleName}</h5>
        <NamedInput
          item={this.editingItem}
          description="Nome"
          fieldName="Nome"
        />
        <button>Adicionar</button>
      </div>
    );
  }
}

export default Node;
