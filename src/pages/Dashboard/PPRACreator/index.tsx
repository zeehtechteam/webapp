import * as React from "react";
import Geral from "./Geral";
import Setores from "./Setores";
import Breadcrumb from "./components/Breadcrumb";
import Navigator from "./components/Navigator";
import { css } from "emotion";
import PPRACreatorStore, { Setor } from "./store";
import { Provider, observer, inject } from "mobx-react";
import Node from "./components/node";

const form = css`
  margin: 20px;
  width: 40%;
`;

type State = {
  etapa: number;
};

const Etapas = [
  "Geral",
  "Setores",
  "Funções",
  "Riscos Ocupacionais",
  "Agentes de Risco"
];

@inject("ppraCreator")
@observer
class PPRACreator extends React.Component<
  { ppraCreator?: typeof PPRACreatorStore },
  State
> {
  initialState: State = {
    etapa: 0
  };
  state = this.initialState;
  nextEtapa = () => {
    this.setState(({ etapa }) => ({ etapa: etapa + 1 }));
  };
  previousEtapa = () => {
    this.setState(({ etapa }) => ({ etapa: etapa - 1 }));
  };
  render() {
    const { etapa } = this.state;
    const { ppraCreator } = this.props;
    const editingItem = Setor.create({
      Nome: "",
      Funcoes: []
    });
    return (
      <div>
        {/* <Breadcrumb paths={Etapas} pathIndex={etapa} /> */}
        {/* <div className={form}>{etapa === 0 ? <Geral /> : <Setores />}</div> */}

        <Node
          item={ppraCreator.Setores}
          type={Setor}
          editingItem={editingItem}
        />

        <Navigator
          onNext={etapa < Etapas.length - 1 && this.nextEtapa}
          onPrevious={etapa > 0 && this.previousEtapa}
        />
      </div>
    );
  }
}

const withState: React.SFC = () => (
  <Provider ppraCreator={PPRACreatorStore}>
    <PPRACreator />
  </Provider>
);

export default withState;
