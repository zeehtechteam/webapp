import { types } from "mobx-state-tree";

const AgenteDeRisco = types.model("AgenteDeRisco", {
  Nome: types.string,
  FormaDeAvaliacao: types.array(types.string),
  ValoresEncontrados: types.array(types.string),
  FontesGeradoras: types.array(types.string),
  ConsequenciasPossiveis: types.array(types.string),
  TiposDeExposicao: types.array(types.string),
  MeiosDeControleExistentes: types.array(types.string),
  MeiosDeControlePropostos: types.array(types.string)
});

const RiscoOcupacional = types.model("RiscoOcupacional", {
  Nome: types.string,
  AgentesDeRisco: types.array(AgenteDeRisco)
});

const Funcao = types
  .model("Funcao", {
    Nome: types.string,
    RiscosOcupacionais: types.array(RiscoOcupacional)
  })
  .actions(self => ({
    add: (nome: string) =>
      self.RiscosOcupacionais.push(
        RiscoOcupacional.create({ Nome: nome, AgentesDeRisco: [] })
      )
  }));

const Setor = types
  .model("Setor", {
    Nome: types.optional(types.string, ""),
    Funcoes: types.optional(types.array(Funcao), [])
  })
  .actions(self => ({
    add: (nome: string) =>
      self.Funcoes.push(Funcao.create({ Nome: nome, RiscosOcupacionais: [] })),
    setNome: (nome: string) => (self.Nome = nome)
  }));

const PPRACreatorStore = types
  .model("PPRACreatorStore", {
    Empresa: types.string,
    QuantidadeDeFuncionarios: types.number,
    Setores: types.array(Setor)
  })
  .actions(self => ({
    addSetor: (nome: string) =>
      self.Setores.push(Setor.create({ Nome: nome, Funcoes: [] }))
  }));

export { AgenteDeRisco, RiscoOcupacional, Funcao, Setor, PPRACreatorStore };
export default PPRACreatorStore.create({
  Empresa: "",
  QuantidadeDeFuncionarios: 0,
  Setores: []
});
