export type AgenteDeRisco = {
  Nome: string;
  FormaDeAvaliacao: string[];
  ValoresEncontrados: string[];
  FontesGeradoras: string[];
  ConsequenciasPossiveis: string[];
  TiposDeExposicao: string[];
  MeiosDeControleExistentes: string[];
  MeiosDeControlePropostos: string[];
};

export type RiscoOcupacional = {
  Nome: string;
  AgentesDeRisco: AgenteDeRisco[];
};

export type Funcao = {
  Nome: string;
  RiscosOcupacionais: RiscoOcupacional[];
};

export type Setor = {
  Nome: string;
  Funcoes: Funcao[];
};
