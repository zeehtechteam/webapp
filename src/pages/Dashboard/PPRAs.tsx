import * as React from "react";
import { inject, observer } from "mobx-react";
import { css } from "emotion";
import DashboardStore from "./store";
import { FaSearch, FaCopy } from "react-icons/fa";
import PPRACreator from "./PPRACreator";

const container = css`
  //background: yellow;
  display: flex;
  flex-direction: column;
`;

const resumeCard = css`
  padding: 20px;
  background: #ededed;
  border: 1px solid rgba(0, 0, 0, 0.1);
  align-self: flex-start;
  & > p {
    color: #000;
    & > span {
      font-weight: 1000;
      font-size: 18px;
    }
  }
`;

const ppraHistory = css`
  margin-top: 20px;
  & > table {
    display: flex;
    flex-direction: column;
    & > thead {
      width: 100%;
      & > tr {
        display: flex;
        & > th {
          width: calc(100% / 3);
        }
      }
    }
    & > tbody {
      width: 100%;
      & > tr {
        display: flex;
        & > td {
          width: calc(100% / 3);
          text-align: start;
        }
      }
    }
  }
`;

const operations = css`
  margin-top: 20px;
  display: flex;
`;

type Props = {
  dashboardStore?: DashboardStore;
};
type State = {
  creatingPPRA: boolean;
};

@inject("dashboardStore")
@observer
class PPRAs extends React.Component<Props, State> {
  initialState: State = {
    creatingPPRA: false
  };
  state = this.initialState;

  novoPPRA = () =>
    this.setState(
      { creatingPPRA: true },
      () => (this.props.dashboardStore.menuBlocked = true)
    );

  render() {
    const { creatingPPRA } = this.state;
    if (creatingPPRA) {
      return <PPRACreator />;
    }
    const {
      items: { ppras }
    } = this.props.dashboardStore;
    return (
      <div className={container}>
        <div className={resumeCard}>
          <p>
            Você possui <span>{ppras}</span> PPRAs disponíveis para impressão.
          </p>
        </div>
        <div className={operations}>
          <button className="button is-success" onClick={this.novoPPRA}>
            Criar novo PPRA
          </button>
        </div>
        <div className={ppraHistory}>
          <header>
            <h4>Histórico</h4>
          </header>
          <table className="table is-narrow">
            <thead>
              <tr>
                <th>Data</th>
                <th>Empresa</th>
                <th>Operações</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
              <tr>
                <td>01/01/2018 14:00</td>
                <td>Teste</td>
                <td>
                  <button className="button is-info is-small">
                    <FaSearch />
                    &nbsp; Visualizar
                  </button>
                  <button className="button is-warning is-small">
                    <FaCopy /> &nbsp; Replicar
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default PPRAs;
