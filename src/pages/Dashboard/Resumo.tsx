import * as React from "react";
import { inject, observer } from "mobx-react";
import { css } from "emotion";
import DashboardStore from "./store";

type Props = {
  dashboardStore?: DashboardStore;
};

const container = css``;

@inject("dashboardStore")
@observer
class Resumo extends React.Component<Props> {
  render() {
    return <div className={container}>Resumo!</div>;
  }
}

export default Resumo;
