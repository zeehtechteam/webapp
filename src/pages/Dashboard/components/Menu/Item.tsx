import * as React from "react";
import { IconType } from "react-icons/lib/iconBase";

type Props = {
  name: string;
  icon: JSX.Element;
  description: string;
  activeName: string;
  changeItem: (item: string) => void;
};
const MenuItem: React.SFC<Props> = ({
  name,
  icon,
  description,
  activeName,
  changeItem
}) => (
  <React.Fragment>
    <a
      className={activeName === name ? "active" : ""}
      onClick={() => changeItem(name)}
    >
      {icon}
      <span>{description}</span>
    </a>
  </React.Fragment>
);

export default MenuItem;
