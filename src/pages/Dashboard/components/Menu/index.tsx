import * as React from "react";
import { css } from "emotion";
import { FaInfo, FaHistory, FaUser, FaCreditCard } from "react-icons/fa";
import MenuItem from "./Item";
import { inject, observer } from "mobx-react";
import DashboardStore from "../../store";

const container = css`
  position: fixed;
  height: 100%;
  background: #323031;
  display: flex;
  //grid-template-columns: 1fr;
  flex-direction: column;
  width: 200px;
  //padding-bottom: 20px;
  padding: 0px 5px;
  padding-bottom: 5px;
  & > header {
    color: #fff;
    padding: 20px 0px;
    border-bottom: 1px solid #222021;
    display: flex;
    flex-direction: column;
    & > h1 {
      font-size: 26px;
      font-weight: 300;
      text-transform: uppercase;
      text-align: center;
    }
    & > small {
      align-self: center;
    }
  }
  & > a {
    color: #fff;
    padding: 10px 20px;
    border-top: none;
    border-bottom: 1px solid #222021;
    border-left: 1px solid #222021;
    border-right: 1px solid #222021;
    &:hover {
      background: #424041;
    }
    &.active {
      background: #525051;
    }
    & > span {
      margin-left: 10px;
      font-size: 16px;
    }
  }
  & > footer {
    display: flex;
    flex-direction: column;
    margin-top: auto;
    & > small {
      align-self: center;
    }
  }
  & > .menu-block {
    position: fixed;
    height: 100%;
    width: 200px;
    background: rgba(255, 255, 255, 0.1);
    cursor: not-allowed;
  }
`;

type Props = {
  onChangeItem: (item: string) => void;
  active: string;
  dashboardStore?: DashboardStore;
};

@inject("dashboardStore")
@observer
class Menu extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    const { onChangeItem, active } = this.props;
    const { menuBlocked } = this.props.dashboardStore;
    return (
      <div className={container}>
        <header>
          <h1>Dashboard</h1>
          <small>Olá, {this.props.dashboardStore!.name}!</small>
        </header>
        <MenuItem
          name="resumo"
          icon={<FaInfo />}
          description="Resumo"
          activeName={active}
          changeItem={onChangeItem}
        />
        <MenuItem
          name="conta"
          icon={<FaUser />}
          description="Conta"
          activeName={active}
          changeItem={onChangeItem}
        />
        <MenuItem
          name="ppras"
          icon={<FaHistory />}
          description="PPRAs"
          activeName={active}
          changeItem={onChangeItem}
        />
        <MenuItem
          name="pcmsos"
          icon={<FaHistory />}
          description="PCMSOs"
          activeName={active}
          changeItem={onChangeItem}
        />
        <MenuItem
          name="transacoes"
          icon={<FaCreditCard />}
          description="Transações"
          activeName={active}
          changeItem={onChangeItem}
        />
        <footer>
          <small>Copyright 2018</small>
        </footer>
        {menuBlocked && <div className="menu-block" />}
      </div>
    );
  }
}

export default Menu;
