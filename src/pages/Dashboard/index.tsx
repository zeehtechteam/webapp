import * as React from "react";
import { observer, inject, Provider } from "mobx-react";
import { setTitle } from "../../libs/titleManager";
import { css } from "emotion";
import Menu from "./components/Menu";
import { asyncComponent } from "react-async-component";
import DashboardStore from "./store";
import UserInfo from "../../graphql/queries/UserInfo";
import { Query } from "react-apollo";
// import Resumo from "./Resumo";
// import Conta from "./Conta";
// import PPRAs from "./PPRAs";
// import PCMSOs from "./PCMSOs";
// import Transacoes from "./Transacoes";

const Route: React.SFC<{ page: string }> = ({ page }) => {
  switch (page) {
    case "resumo":
      return <Resumo />;
    case "conta":
      return <Conta />;
    case "ppras":
      return <PPRAs />;
    case "pcmsos":
      return <PCMSOs />;
    case "transacoes":
      return <Transacoes />;
  }
};

const Loading = () => <div>Aguarde um momento...</div>;

const Resumo = asyncComponent({
  resolve: () => import("./Resumo"),
  LoadingComponent: Loading
});

const Conta = asyncComponent({
  resolve: () => import("./Conta"),
  LoadingComponent: Loading
});

const PPRAs = asyncComponent({
  resolve: () => import("./PPRAs"),
  LoadingComponent: Loading
});

const PCMSOs = asyncComponent({
  resolve: () => import("./PCMSOs"),
  LoadingComponent: Loading
});

const Transacoes = asyncComponent({
  resolve: () => import("./Transacoes"),
  LoadingComponent: Loading
});

const container = css`
  box-sizing: border-box;
  display: grid;
  grid-template-areas:
    "navbar navbar navbar"
    "menu content content";
  grid-template-columns: 220px 1fr 1fr;
  grid-template-rows: auto auto;

  & > .menu {
    grid-area: menu;
    height: 100%;
  }
  & > .navbar {
    grid-area: navbar;
    background-color: blue;
    color: white;
  }
  & > .content {
    grid-area: content;
    margin-top: 20px;
  }
`;

type Props = {};

type State = {
  page: string;
};

@inject("userStore")
@observer
class Dashboard extends React.Component<Props, State> {
  store = new DashboardStore();
  constructor(props: Props) {
    super(props);
    setTitle("Dashboard");
    this.state = {
      page: "ppras"
    };
  }

  changePage = (item: string) => {
    this.setState({ page: item });
  };

  render() {
    const { page } = this.state;

    return (
      <Provider dashboardStore={this.store}>
        <div className={container}>
          <div className="menu">
            <Menu onChangeItem={this.changePage} active={page} />
          </div>
          <div className="content">
            <Query query={UserInfo}>
              {({ loading, error, data, fetchMore }) => {
                if (loading) {
                  return <div>Carregando...</div>;
                }
                if (error) {
                  return <div>Algum erro ocorreu!</div>;
                }
                this.store.name = data.userInfo.name;
                this.store.email = data.userInfo.email;
                this.store.items = data.userInfo.items;
                this.store.phone = data.userInfo.phone;
                this.store.transactions = data.userInfo.transactions;
                this.store.refetch = fetchMore;
                return <Route page={page} />;
              }}
            </Query>
          </div>
        </div>
      </Provider>
    );
  }
}

export default Dashboard;
