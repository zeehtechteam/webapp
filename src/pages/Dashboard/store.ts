import { observable } from "mobx";

type Items = {
  ppras: number;
  pcmsos: number;
};

type Transaction = {
  id: number;
  date: Date;
  approvedDate: Date;
  status: number;
};

class DashboardStore {
  @observable name: string;
  @observable email: string;
  @observable phone: string;
  @observable items: Items;
  @observable transactions: Transaction[];
  @observable loading: boolean;
  @observable refetch: any;
  @observable menuBlocked: boolean;
  constructor() {
    this.name = "";
    this.email = "";
    this.phone = "";
    this.items = { ppras: 0, pcmsos: 0 };
    this.transactions = [];
    this.loading = false;
    this.menuBlocked = false;
  }
}

export default DashboardStore;
