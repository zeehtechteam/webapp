import * as React from 'react';
import {observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import HomePageStore from './store';
import Modal from '../../components/Modal';
import {setTitle} from '../../libs/titleManager';
import Empresas from '../../forms/Empresas';

@observer
class HomePage extends React.Component<any, any> {
  public store: HomePageStore;
  constructor(props: any) {
    super(props);
    this.store = new HomePageStore();
  }
  render() {
    const store = this.store;
    setTitle('Início');
    return (
      <div>
        <button type="button" className="button is-success" onClick={() => store.modal = Empresas}>Abrir Modal Empresas</button>
        {store.modal !== undefined ?
          <Modal form={store.modal} onClose={() => store.modal = undefined}/>
        : <div></div>
        }
      </div> 
    );
  }
}

export default HomePage;