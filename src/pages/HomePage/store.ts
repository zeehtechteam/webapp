import {observable, action, computed} from 'mobx';
import Form from '../../form-management/Form';

class HomePageStore {
  @observable public nome: string;
  @observable public idade: string;
  @observable public modal: Form;
}

export default HomePageStore;