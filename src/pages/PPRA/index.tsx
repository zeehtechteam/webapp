import * as React from "react";
import { observer } from "mobx-react";
import PPRAStore from "./store";
import { setTitle } from "../../libs/titleManager";

type Props = {};

@observer
class PPRA extends React.Component<Props> {
  public store: PPRAStore;
  constructor(props: Props) {
    super(props);
    this.store = new PPRAStore();
  }

  render() {
    const store = this.store;
    return (
      <div>
        Hello {store.nome}!<br />
        <br />
        <button
          type="button"
          className="button is-success"
          onClick={() => (store.nome = "Zeeh")}
        >
          Teste
        </button>
      </div>
    );
  }
}

export default PPRA;
