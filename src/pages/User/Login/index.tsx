import * as React from "react";
import { observer, inject } from "mobx-react";
import LoginStore from "./store";
import UserStore from "../../../global-stores/UserStore";
import { Redirect } from "react-router";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";

const LOGIN_MUTATION = gql`
  mutation Login($login: String!, $password: String!) {
    login(login: $login, password: $password)
  }
`;

export interface LoginProps {
  userStore?: UserStore;
}

@inject("userStore")
@observer
class Login extends React.Component<LoginProps> {
  public store: LoginStore;
  constructor(props: LoginProps) {
    super(props);
    this.store = new LoginStore();
    this.props.userStore.logout();
    console.log("hey");
  }

  confirmLogin = (data: any) => {
    if (data.login) {
      this.props.userStore.loggedIn = true;
      localStorage.setItem("auth", data.login);
    }
    this.store.loading = false;
  };

  onLogin = (mutation: any) => {
    this.store.loading = true;
    mutation();
  };

  render() {
    const store = this.store;
    if (this.props.userStore.loggedIn === true) {
      return <Redirect to="/dashboard" />;
    }
    return (
      <div>
        <section className="hero is-fullheight">
          <div className="hero-body">
            <div className="container has-text-centered">
              <div className="column is-6 is-offset-3">
                <h3 className="title">Login</h3>
                <p className="subtitle">Por favor, logue-se para prosseguir.</p>
                <div className="box">
                  <div>
                    {store.errorMessage !== "" && (
                      <div className="notification is-warning">
                        {store.errorMessage}
                      </div>
                    )}
                    <div className="field">
                      <div className="control">
                        <input
                          className="input"
                          type="email"
                          placeholder="Email"
                          value={store.usuario}
                          onChange={e => (store.usuario = e.target.value)}
                          autoFocus
                        />
                      </div>
                    </div>

                    <div className="field">
                      <div className="control">
                        <input
                          className="input"
                          type="password"
                          placeholder="Senha"
                          value={store.senha}
                          onChange={e => (store.senha = e.target.value)}
                        />
                      </div>
                    </div>
                    <div className="field">
                      <label className="checkbox">
                        <input
                          type="checkbox"
                          onChange={e => (store.lembrarme = e.target.checked)}
                        />
                        &nbsp; Lembrar-me
                      </label>
                    </div>
                    <Mutation
                      mutation={LOGIN_MUTATION}
                      variables={{
                        login: store.usuario,
                        password: store.senha
                      }}
                      onCompleted={data => this.confirmLogin(data)}
                    >
                      {mutation => (
                        <button
                          className={
                            "button is-block is-info is-large is-fullwidth" +
                            (this.store.loading === true ? " is-loading" : "")
                          }
                          onClick={() => this.onLogin(mutation)}
                        >
                          Login
                        </button>
                      )}
                    </Mutation>
                  </div>
                </div>
                <p className="has-text-grey">
                  <a href="../">Cadastre-se</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                  <a href="../">Esqueceu sua senha?</a>
                </p>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Login;
