import {observable} from 'mobx';

class LoginStore {
  @observable public usuario: string;
  @observable public senha: string;
  @observable public lembrarme: boolean;
  @observable public errorMessage: string;
  @observable public loading: boolean;
  @observable public loginSuccess: boolean;
  constructor() {
    this.usuario = '';
    this.senha = '';
    this.errorMessage = '';
  }
}

export default LoginStore;