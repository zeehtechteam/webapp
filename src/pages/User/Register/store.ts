import {observable} from 'mobx';

class RegisterStore {
  @observable public usuario: string;
  @observable public senha: string;
  @observable public nome: string;
  @observable public telefone: string;
  @observable public errorMessage: string;
  @observable public loading: boolean;
  constructor() {
    this.usuario = '';
    this.senha = '';
    this.nome = '';
    this.telefone = '';
    this.errorMessage = '';
    this.loading = false;
  }
}

export default RegisterStore;